'use strict'

const images = document.querySelectorAll('.image-to-show');
const delay = 3000;
const btnStop = document.querySelector(".button_stop");
const btnRestore = document.querySelector('.button_restore');
const timerBlock = document.querySelector('.timer');

let pointer = 1;
let intervalId;

function queue() {
    timer();

    intervalId = setInterval(function() {
        timer();

        if (pointer === images.length) {
            pointer = 0;
        }

        images.forEach((image, index) => {
            if (pointer === index) {
                image.style.display='flex'
            } else {
                image.style.display='none'
            }
        })

        btnStop.style.display='inline-block';
        btnRestore.style.display='inline-block';

        pointer++;
    }, delay);
}

let timerID;
function timer() {
    let timerDelay = delay;
    let iterationTime = 100;

    clearInterval(timerID);
    timerID = setInterval(function () {
        timerBlock.innerText = (timerDelay / 1000);
        timerDelay = timerDelay - iterationTime;
    }, iterationTime)
}

queue();

btnStop.addEventListener('click', (event) => {
    clearInterval(intervalId);
    clearInterval(timerID);
});

btnRestore.addEventListener('click', (event) => {
    queue();
});
